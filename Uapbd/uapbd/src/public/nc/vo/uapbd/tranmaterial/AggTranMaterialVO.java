package nc.vo.uapbd.tranmaterial;

import nc.vo.pubapp.pattern.model.entity.bill.AbstractBill;
import nc.vo.pubapp.pattern.model.meta.entity.bill.BillMetaFactory;
import nc.vo.pubapp.pattern.model.meta.entity.bill.IBillMeta;

@nc.vo.annotation.AggVoInfo(parentVO = "nc.vo.uapbd.tranmaterial.TranMaterialVO")

public class AggTranMaterialVO extends AbstractBill {
	
	  @Override
	  public IBillMeta getMetaData() {
	  	IBillMeta billMeta =BillMetaFactory.getInstance().getBillMeta(AggTranMaterialVOMeta.class);
	  	return billMeta;
	  }
	    
	  @Override
	  public TranMaterialVO getParentVO(){
	  	return (TranMaterialVO)this.getParent();
	  }
	  
}