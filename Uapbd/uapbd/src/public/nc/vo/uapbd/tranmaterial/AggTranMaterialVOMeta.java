package nc.vo.uapbd.tranmaterial;

import nc.vo.pubapp.pattern.model.meta.entity.bill.AbstractBillMeta;

public class AggTranMaterialVOMeta extends AbstractBillMeta{
	
	public AggTranMaterialVOMeta(){
		this.init();
	}
	
	private void init() {
		this.setParent(nc.vo.uapbd.tranmaterial.TranMaterialVO.class);
	}
}