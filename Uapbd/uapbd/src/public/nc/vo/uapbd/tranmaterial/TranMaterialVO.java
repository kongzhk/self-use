package nc.vo.uapbd.tranmaterial;

import nc.vo.pub.IVOMeta;
import nc.vo.pub.SuperVO;
import nc.vo.pub.lang.UFBoolean;
import nc.vo.pub.lang.UFDate;
import nc.vo.pub.lang.UFDateTime;
import nc.vo.pub.lang.UFDouble;
import nc.vo.pubapp.pattern.model.meta.entity.vo.VOMetaFactory;

/**
 * <b> 此处简要描述此类功能 </b>
 * <p>
 *   此处添加累的描述信息
 * </p>
 *  创建日期:2021-12-22
 * @author yonyouBQ
 * @version NCPrj ??
 */
 
public class TranMaterialVO extends SuperVO {
	
/**
*主键
*/
public String pk_tranmaterial;
/**
*创建人
*/
public String creator;
/**
*创建时间
*/
public UFDateTime creationtime;
/**
*修改人
*/
public String modifier;
/**
*修改时间
*/
public UFDateTime modifiedtime;
/**
*业务类型
*/
public String busitype;
/**
*制单人
*/
public String billmaker;
/**
*审批人
*/
public String approver;
/**
*审批状态
*/
public Integer approvestatus;
/**
*审批批语
*/
public String approvenote;
/**
*审批时间
*/
public UFDateTime approvedate;
/**
*交易类型
*/
public String transtype;
/**
*单据类型
*/
public String billtype;
/**
*交易类型pk
*/
public String transtypepk;
/**
*来源单据类型
*/
public String srcbilltype;
/**
*来源单据id
*/
public String srcbillid;
/**
*修订枚举
*/
public Integer emendenum;
/**
*单据版本pk
*/
public String billversionpk;
/**
*编码
*/
public String code;
/**
*名称
*/
public String name;
/**
*制单时间
*/
public UFDateTime maketime;
/**
*最后修改时间
*/
public UFDateTime lastmaketime;
/**
*集团
*/
public String pk_group;
/**
*组织
*/
public String pk_org;
/**
*组织版本
*/
public String pk_org_v;
/**
*单据日期
*/
public UFDate dbilldate;
/**
*自定义项1
*/
public String vdef1;
/**
*自定义项2
*/
public String vdef2;
/**
*自定义项3
*/
public String vdef3;
/**
*自定义项4
*/
public String vdef4;
/**
*自定义项5
*/
public String vdef5;
/**
*自定义项6
*/
public String vdef6;
/**
*自定义项7
*/
public String vdef7;
/**
*自定义项8
*/
public String vdef8;
/**
*自定义项9
*/
public String vdef9;
/**
*自定义项10
*/
public String vdef10;
/**
*物料
*/
public String cmaterialoid;
/**
*物料版本
*/
public String cmaterialvid;
/**
*实发主数量
*/
public String nnum;
/**
*主单位
*/
public String cunitid;
/**
*单位
*/
public String castunitid;
/**
*仓库
*/
public String cwarehouseid;
/**
*货位
*/
public String clocationid;
/**
*部门
*/
public String cdptid;
/**
*部门版本
*/
public String cdptvid;
/**
*实发数量
*/
public String nassistnum;
/**
*来源单据号
*/
public String csourcebillcode;
/**
*来源单据主键
*/
public String csourcebillhid;
/**
*来源单据行主键
*/
public String csourcebillbid;
/**
*生产订单号
*/
public String vsourcemocode;
/**
*生产订单主键
*/
public String vsourcemoid;
/**
*生产订单明细主键
*/
public String vsourcemorowid;
/**
*产品编码
*/
public String pcmaterialvid;
/**
*挪入备料计划
*/
public String pk_pickm;
/**
*挪入部门
*/
public String in_dept;
/**
*时间戳
*/
public UFDateTime ts;
    
    
/**
* 属性 pk_tranmaterial的Getter方法.属性名：主键
*  创建日期:2021-12-22
* @return java.lang.String
*/
public String getPk_tranmaterial() {
return this.pk_tranmaterial;
} 

/**
* 属性pk_tranmaterial的Setter方法.属性名：主键
* 创建日期:2021-12-22
* @param newPk_tranmaterial java.lang.String
*/
public void setPk_tranmaterial ( String pk_tranmaterial) {
this.pk_tranmaterial=pk_tranmaterial;
} 
 
/**
* 属性 creator的Getter方法.属性名：创建人
*  创建日期:2021-12-22
* @return nc.vo.sm.UserVO
*/
public String getCreator() {
return this.creator;
} 

/**
* 属性creator的Setter方法.属性名：创建人
* 创建日期:2021-12-22
* @param newCreator nc.vo.sm.UserVO
*/
public void setCreator ( String creator) {
this.creator=creator;
} 
 
/**
* 属性 creationtime的Getter方法.属性名：创建时间
*  创建日期:2021-12-22
* @return nc.vo.pub.lang.UFDateTime
*/
public UFDateTime getCreationtime() {
return this.creationtime;
} 

/**
* 属性creationtime的Setter方法.属性名：创建时间
* 创建日期:2021-12-22
* @param newCreationtime nc.vo.pub.lang.UFDateTime
*/
public void setCreationtime ( UFDateTime creationtime) {
this.creationtime=creationtime;
} 
 
/**
* 属性 modifier的Getter方法.属性名：修改人
*  创建日期:2021-12-22
* @return nc.vo.sm.UserVO
*/
public String getModifier() {
return this.modifier;
} 

/**
* 属性modifier的Setter方法.属性名：修改人
* 创建日期:2021-12-22
* @param newModifier nc.vo.sm.UserVO
*/
public void setModifier ( String modifier) {
this.modifier=modifier;
} 
 
/**
* 属性 modifiedtime的Getter方法.属性名：修改时间
*  创建日期:2021-12-22
* @return nc.vo.pub.lang.UFDateTime
*/
public UFDateTime getModifiedtime() {
return this.modifiedtime;
} 

/**
* 属性modifiedtime的Setter方法.属性名：修改时间
* 创建日期:2021-12-22
* @param newModifiedtime nc.vo.pub.lang.UFDateTime
*/
public void setModifiedtime ( UFDateTime modifiedtime) {
this.modifiedtime=modifiedtime;
} 
 
/**
* 属性 busitype的Getter方法.属性名：业务类型
*  创建日期:2021-12-22
* @return java.lang.String
*/
public String getBusitype() {
return this.busitype;
} 

/**
* 属性busitype的Setter方法.属性名：业务类型
* 创建日期:2021-12-22
* @param newBusitype java.lang.String
*/
public void setBusitype ( String busitype) {
this.busitype=busitype;
} 
 
/**
* 属性 billmaker的Getter方法.属性名：制单人
*  创建日期:2021-12-22
* @return java.lang.String
*/
public String getBillmaker() {
return this.billmaker;
} 

/**
* 属性billmaker的Setter方法.属性名：制单人
* 创建日期:2021-12-22
* @param newBillmaker java.lang.String
*/
public void setBillmaker ( String billmaker) {
this.billmaker=billmaker;
} 
 
/**
* 属性 approver的Getter方法.属性名：审批人
*  创建日期:2021-12-22
* @return java.lang.String
*/
public String getApprover() {
return this.approver;
} 

/**
* 属性approver的Setter方法.属性名：审批人
* 创建日期:2021-12-22
* @param newApprover java.lang.String
*/
public void setApprover ( String approver) {
this.approver=approver;
} 
 
/**
* 属性 approvestatus的Getter方法.属性名：审批状态
*  创建日期:2021-12-22
* @return nc.vo.pub.pf.BillStatusEnum
*/
public Integer getApprovestatus() {
return this.approvestatus;
} 

/**
* 属性approvestatus的Setter方法.属性名：审批状态
* 创建日期:2021-12-22
* @param newApprovestatus nc.vo.pub.pf.BillStatusEnum
*/
public void setApprovestatus ( Integer approvestatus) {
this.approvestatus=approvestatus;
} 
 
/**
* 属性 approvenote的Getter方法.属性名：审批批语
*  创建日期:2021-12-22
* @return java.lang.String
*/
public String getApprovenote() {
return this.approvenote;
} 

/**
* 属性approvenote的Setter方法.属性名：审批批语
* 创建日期:2021-12-22
* @param newApprovenote java.lang.String
*/
public void setApprovenote ( String approvenote) {
this.approvenote=approvenote;
} 
 
/**
* 属性 approvedate的Getter方法.属性名：审批时间
*  创建日期:2021-12-22
* @return nc.vo.pub.lang.UFDateTime
*/
public UFDateTime getApprovedate() {
return this.approvedate;
} 

/**
* 属性approvedate的Setter方法.属性名：审批时间
* 创建日期:2021-12-22
* @param newApprovedate nc.vo.pub.lang.UFDateTime
*/
public void setApprovedate ( UFDateTime approvedate) {
this.approvedate=approvedate;
} 
 
/**
* 属性 transtype的Getter方法.属性名：交易类型
*  创建日期:2021-12-22
* @return java.lang.String
*/
public String getTranstype() {
return this.transtype;
} 

/**
* 属性transtype的Setter方法.属性名：交易类型
* 创建日期:2021-12-22
* @param newTranstype java.lang.String
*/
public void setTranstype ( String transtype) {
this.transtype=transtype;
} 
 
/**
* 属性 billtype的Getter方法.属性名：单据类型
*  创建日期:2021-12-22
* @return java.lang.String
*/
public String getBilltype() {
return this.billtype;
} 

/**
* 属性billtype的Setter方法.属性名：单据类型
* 创建日期:2021-12-22
* @param newBilltype java.lang.String
*/
public void setBilltype ( String billtype) {
this.billtype=billtype;
} 
 
/**
* 属性 transtypepk的Getter方法.属性名：交易类型pk
*  创建日期:2021-12-22
* @return java.lang.String
*/
public String getTranstypepk() {
return this.transtypepk;
} 

/**
* 属性transtypepk的Setter方法.属性名：交易类型pk
* 创建日期:2021-12-22
* @param newTranstypepk java.lang.String
*/
public void setTranstypepk ( String transtypepk) {
this.transtypepk=transtypepk;
} 
 
/**
* 属性 srcbilltype的Getter方法.属性名：来源单据类型
*  创建日期:2021-12-22
* @return java.lang.String
*/
public String getSrcbilltype() {
return this.srcbilltype;
} 

/**
* 属性srcbilltype的Setter方法.属性名：来源单据类型
* 创建日期:2021-12-22
* @param newSrcbilltype java.lang.String
*/
public void setSrcbilltype ( String srcbilltype) {
this.srcbilltype=srcbilltype;
} 
 
/**
* 属性 srcbillid的Getter方法.属性名：来源单据id
*  创建日期:2021-12-22
* @return java.lang.String
*/
public String getSrcbillid() {
return this.srcbillid;
} 

/**
* 属性srcbillid的Setter方法.属性名：来源单据id
* 创建日期:2021-12-22
* @param newSrcbillid java.lang.String
*/
public void setSrcbillid ( String srcbillid) {
this.srcbillid=srcbillid;
} 
 
/**
* 属性 emendenum的Getter方法.属性名：修订枚举
*  创建日期:2021-12-22
* @return java.lang.Integer
*/
public Integer getEmendenum() {
return this.emendenum;
} 

/**
* 属性emendenum的Setter方法.属性名：修订枚举
* 创建日期:2021-12-22
* @param newEmendenum java.lang.Integer
*/
public void setEmendenum ( Integer emendenum) {
this.emendenum=emendenum;
} 
 
/**
* 属性 billversionpk的Getter方法.属性名：单据版本pk
*  创建日期:2021-12-22
* @return java.lang.String
*/
public String getBillversionpk() {
return this.billversionpk;
} 

/**
* 属性billversionpk的Setter方法.属性名：单据版本pk
* 创建日期:2021-12-22
* @param newBillversionpk java.lang.String
*/
public void setBillversionpk ( String billversionpk) {
this.billversionpk=billversionpk;
} 
 
/**
* 属性 code的Getter方法.属性名：编码
*  创建日期:2021-12-22
* @return java.lang.String
*/
public String getCode() {
return this.code;
} 

/**
* 属性code的Setter方法.属性名：编码
* 创建日期:2021-12-22
* @param newCode java.lang.String
*/
public void setCode ( String code) {
this.code=code;
} 
 
/**
* 属性 name的Getter方法.属性名：名称
*  创建日期:2021-12-22
* @return java.lang.String
*/
public String getName() {
return this.name;
} 

/**
* 属性name的Setter方法.属性名：名称
* 创建日期:2021-12-22
* @param newName java.lang.String
*/
public void setName ( String name) {
this.name=name;
} 
 
/**
* 属性 maketime的Getter方法.属性名：制单时间
*  创建日期:2021-12-22
* @return nc.vo.pub.lang.UFDateTime
*/
public UFDateTime getMaketime() {
return this.maketime;
} 

/**
* 属性maketime的Setter方法.属性名：制单时间
* 创建日期:2021-12-22
* @param newMaketime nc.vo.pub.lang.UFDateTime
*/
public void setMaketime ( UFDateTime maketime) {
this.maketime=maketime;
} 
 
/**
* 属性 lastmaketime的Getter方法.属性名：最后修改时间
*  创建日期:2021-12-22
* @return nc.vo.pub.lang.UFDateTime
*/
public UFDateTime getLastmaketime() {
return this.lastmaketime;
} 

/**
* 属性lastmaketime的Setter方法.属性名：最后修改时间
* 创建日期:2021-12-22
* @param newLastmaketime nc.vo.pub.lang.UFDateTime
*/
public void setLastmaketime ( UFDateTime lastmaketime) {
this.lastmaketime=lastmaketime;
} 
 
/**
* 属性 pk_group的Getter方法.属性名：集团
*  创建日期:2021-12-22
* @return nc.vo.org.GroupVO
*/
public String getPk_group() {
return this.pk_group;
} 

/**
* 属性pk_group的Setter方法.属性名：集团
* 创建日期:2021-12-22
* @param newPk_group nc.vo.org.GroupVO
*/
public void setPk_group ( String pk_group) {
this.pk_group=pk_group;
} 
 
/**
* 属性 pk_org的Getter方法.属性名：组织
*  创建日期:2021-12-22
* @return nc.vo.org.OrgVO
*/
public String getPk_org() {
return this.pk_org;
} 

/**
* 属性pk_org的Setter方法.属性名：组织
* 创建日期:2021-12-22
* @param newPk_org nc.vo.org.OrgVO
*/
public void setPk_org ( String pk_org) {
this.pk_org=pk_org;
} 
 
/**
* 属性 pk_org_v的Getter方法.属性名：组织版本
*  创建日期:2021-12-22
* @return nc.vo.vorg.OrgVersionVO
*/
public String getPk_org_v() {
return this.pk_org_v;
} 

/**
* 属性pk_org_v的Setter方法.属性名：组织版本
* 创建日期:2021-12-22
* @param newPk_org_v nc.vo.vorg.OrgVersionVO
*/
public void setPk_org_v ( String pk_org_v) {
this.pk_org_v=pk_org_v;
} 
 
/**
* 属性 dbilldate的Getter方法.属性名：单据日期
*  创建日期:2021-12-22
* @return nc.vo.pub.lang.UFDate
*/
public UFDate getDbilldate() {
return this.dbilldate;
} 

/**
* 属性dbilldate的Setter方法.属性名：单据日期
* 创建日期:2021-12-22
* @param newDbilldate nc.vo.pub.lang.UFDate
*/
public void setDbilldate ( UFDate dbilldate) {
this.dbilldate=dbilldate;
} 
 
/**
* 属性 vdef1的Getter方法.属性名：自定义项1
*  创建日期:2021-12-22
* @return java.lang.String
*/
public String getVdef1() {
return this.vdef1;
} 

/**
* 属性vdef1的Setter方法.属性名：自定义项1
* 创建日期:2021-12-22
* @param newVdef1 java.lang.String
*/
public void setVdef1 ( String vdef1) {
this.vdef1=vdef1;
} 
 
/**
* 属性 vdef2的Getter方法.属性名：自定义项2
*  创建日期:2021-12-22
* @return java.lang.String
*/
public String getVdef2() {
return this.vdef2;
} 

/**
* 属性vdef2的Setter方法.属性名：自定义项2
* 创建日期:2021-12-22
* @param newVdef2 java.lang.String
*/
public void setVdef2 ( String vdef2) {
this.vdef2=vdef2;
} 
 
/**
* 属性 vdef3的Getter方法.属性名：自定义项3
*  创建日期:2021-12-22
* @return java.lang.String
*/
public String getVdef3() {
return this.vdef3;
} 

/**
* 属性vdef3的Setter方法.属性名：自定义项3
* 创建日期:2021-12-22
* @param newVdef3 java.lang.String
*/
public void setVdef3 ( String vdef3) {
this.vdef3=vdef3;
} 
 
/**
* 属性 vdef4的Getter方法.属性名：自定义项4
*  创建日期:2021-12-22
* @return java.lang.String
*/
public String getVdef4() {
return this.vdef4;
} 

/**
* 属性vdef4的Setter方法.属性名：自定义项4
* 创建日期:2021-12-22
* @param newVdef4 java.lang.String
*/
public void setVdef4 ( String vdef4) {
this.vdef4=vdef4;
} 
 
/**
* 属性 vdef5的Getter方法.属性名：自定义项5
*  创建日期:2021-12-22
* @return java.lang.String
*/
public String getVdef5() {
return this.vdef5;
} 

/**
* 属性vdef5的Setter方法.属性名：自定义项5
* 创建日期:2021-12-22
* @param newVdef5 java.lang.String
*/
public void setVdef5 ( String vdef5) {
this.vdef5=vdef5;
} 
 
/**
* 属性 vdef6的Getter方法.属性名：自定义项6
*  创建日期:2021-12-22
* @return java.lang.String
*/
public String getVdef6() {
return this.vdef6;
} 

/**
* 属性vdef6的Setter方法.属性名：自定义项6
* 创建日期:2021-12-22
* @param newVdef6 java.lang.String
*/
public void setVdef6 ( String vdef6) {
this.vdef6=vdef6;
} 
 
/**
* 属性 vdef7的Getter方法.属性名：自定义项7
*  创建日期:2021-12-22
* @return java.lang.String
*/
public String getVdef7() {
return this.vdef7;
} 

/**
* 属性vdef7的Setter方法.属性名：自定义项7
* 创建日期:2021-12-22
* @param newVdef7 java.lang.String
*/
public void setVdef7 ( String vdef7) {
this.vdef7=vdef7;
} 
 
/**
* 属性 vdef8的Getter方法.属性名：自定义项8
*  创建日期:2021-12-22
* @return java.lang.String
*/
public String getVdef8() {
return this.vdef8;
} 

/**
* 属性vdef8的Setter方法.属性名：自定义项8
* 创建日期:2021-12-22
* @param newVdef8 java.lang.String
*/
public void setVdef8 ( String vdef8) {
this.vdef8=vdef8;
} 
 
/**
* 属性 vdef9的Getter方法.属性名：自定义项9
*  创建日期:2021-12-22
* @return java.lang.String
*/
public String getVdef9() {
return this.vdef9;
} 

/**
* 属性vdef9的Setter方法.属性名：自定义项9
* 创建日期:2021-12-22
* @param newVdef9 java.lang.String
*/
public void setVdef9 ( String vdef9) {
this.vdef9=vdef9;
} 
 
/**
* 属性 vdef10的Getter方法.属性名：自定义项10
*  创建日期:2021-12-22
* @return java.lang.String
*/
public String getVdef10() {
return this.vdef10;
} 

/**
* 属性vdef10的Setter方法.属性名：自定义项10
* 创建日期:2021-12-22
* @param newVdef10 java.lang.String
*/
public void setVdef10 ( String vdef10) {
this.vdef10=vdef10;
} 
 
/**
* 属性 cmaterialoid的Getter方法.属性名：物料
*  创建日期:2021-12-22
* @return nc.vo.bd.material.MaterialVO
*/
public String getCmaterialoid() {
return this.cmaterialoid;
} 

/**
* 属性cmaterialoid的Setter方法.属性名：物料
* 创建日期:2021-12-22
* @param newCmaterialoid nc.vo.bd.material.MaterialVO
*/
public void setCmaterialoid ( String cmaterialoid) {
this.cmaterialoid=cmaterialoid;
} 
 
/**
* 属性 cmaterialvid的Getter方法.属性名：物料版本
*  创建日期:2021-12-22
* @return nc.vo.bd.material.MaterialVersionVO
*/
public String getCmaterialvid() {
return this.cmaterialvid;
} 

/**
* 属性cmaterialvid的Setter方法.属性名：物料版本
* 创建日期:2021-12-22
* @param newCmaterialvid nc.vo.bd.material.MaterialVersionVO
*/
public void setCmaterialvid ( String cmaterialvid) {
this.cmaterialvid=cmaterialvid;
} 
 
/**
* 属性 nnum的Getter方法.属性名：实发主数量
*  创建日期:2021-12-22
* @return nc.vo.pub.lang.UFDouble
*/
public String getNnum() {
return this.nnum;
} 

/**
* 属性nnum的Setter方法.属性名：实发主数量
* 创建日期:2021-12-22
* @param newNnum nc.vo.pub.lang.UFDouble
*/
public void setNnum ( String nnum) {
this.nnum=nnum;
} 
 
/**
* 属性 cunitid的Getter方法.属性名：主单位
*  创建日期:2021-12-22
* @return nc.vo.bd.material.measdoc.MeasdocVO
*/
public String getCunitid() {
return this.cunitid;
} 

/**
* 属性cunitid的Setter方法.属性名：主单位
* 创建日期:2021-12-22
* @param newCunitid nc.vo.bd.material.measdoc.MeasdocVO
*/
public void setCunitid ( String cunitid) {
this.cunitid=cunitid;
} 
 
/**
* 属性 castunitid的Getter方法.属性名：单位
*  创建日期:2021-12-22
* @return nc.vo.bd.material.measdoc.MeasdocVO
*/
public String getCastunitid() {
return this.castunitid;
} 

/**
* 属性castunitid的Setter方法.属性名：单位
* 创建日期:2021-12-22
* @param newCastunitid nc.vo.bd.material.measdoc.MeasdocVO
*/
public void setCastunitid ( String castunitid) {
this.castunitid=castunitid;
} 
 
/**
* 属性 cwarehouseid的Getter方法.属性名：仓库
*  创建日期:2021-12-22
* @return nc.vo.bd.stordoc.StordocVO
*/
public String getCwarehouseid() {
return this.cwarehouseid;
} 

/**
* 属性cwarehouseid的Setter方法.属性名：仓库
* 创建日期:2021-12-22
* @param newCwarehouseid nc.vo.bd.stordoc.StordocVO
*/
public void setCwarehouseid ( String cwarehouseid) {
this.cwarehouseid=cwarehouseid;
} 
 
/**
* 属性 clocationid的Getter方法.属性名：货位
*  创建日期:2021-12-22
* @return nc.vo.bd.rack.RackVO
*/
public String getClocationid() {
return this.clocationid;
} 

/**
* 属性clocationid的Setter方法.属性名：货位
* 创建日期:2021-12-22
* @param newClocationid nc.vo.bd.rack.RackVO
*/
public void setClocationid ( String clocationid) {
this.clocationid=clocationid;
} 
 
/**
* 属性 cdptid的Getter方法.属性名：部门
*  创建日期:2021-12-22
* @return nc.vo.org.DeptVO
*/
public String getCdptid() {
return this.cdptid;
} 

/**
* 属性cdptid的Setter方法.属性名：部门
* 创建日期:2021-12-22
* @param newCdptid nc.vo.org.DeptVO
*/
public void setCdptid ( String cdptid) {
this.cdptid=cdptid;
} 
 
/**
* 属性 cdptvid的Getter方法.属性名：部门版本
*  创建日期:2021-12-22
* @return nc.vo.vorg.DeptVersionVO
*/
public String getCdptvid() {
return this.cdptvid;
} 

/**
* 属性cdptvid的Setter方法.属性名：部门版本
* 创建日期:2021-12-22
* @param newCdptvid nc.vo.vorg.DeptVersionVO
*/
public void setCdptvid ( String cdptvid) {
this.cdptvid=cdptvid;
} 
 
/**
* 属性 nassistnum的Getter方法.属性名：实发数量
*  创建日期:2021-12-22
* @return nc.vo.pub.lang.UFDouble
*/
public String getNassistnum() {
return this.nassistnum;
} 

/**
* 属性nassistnum的Setter方法.属性名：实发数量
* 创建日期:2021-12-22
* @param newNassistnum nc.vo.pub.lang.UFDouble
*/
public void setNassistnum ( String nassistnum) {
this.nassistnum=nassistnum;
} 
 
/**
* 属性 csourcebillcode的Getter方法.属性名：来源单据号
*  创建日期:2021-12-22
* @return java.lang.String
*/
public String getCsourcebillcode() {
return this.csourcebillcode;
} 

/**
* 属性csourcebillcode的Setter方法.属性名：来源单据号
* 创建日期:2021-12-22
* @param newCsourcebillcode java.lang.String
*/
public void setCsourcebillcode ( String csourcebillcode) {
this.csourcebillcode=csourcebillcode;
} 
 
/**
* 属性 csourcebillhid的Getter方法.属性名：来源单据主键
*  创建日期:2021-12-22
* @return java.lang.String
*/
public String getCsourcebillhid() {
return this.csourcebillhid;
} 

/**
* 属性csourcebillhid的Setter方法.属性名：来源单据主键
* 创建日期:2021-12-22
* @param newCsourcebillhid java.lang.String
*/
public void setCsourcebillhid ( String csourcebillhid) {
this.csourcebillhid=csourcebillhid;
} 
 
/**
* 属性 csourcebillbid的Getter方法.属性名：来源单据行主键
*  创建日期:2021-12-22
* @return java.lang.String
*/
public String getCsourcebillbid() {
return this.csourcebillbid;
} 

/**
* 属性csourcebillbid的Setter方法.属性名：来源单据行主键
* 创建日期:2021-12-22
* @param newCsourcebillbid java.lang.String
*/
public void setCsourcebillbid ( String csourcebillbid) {
this.csourcebillbid=csourcebillbid;
} 
 
/**
* 属性 vsourcemocode的Getter方法.属性名：生产订单号
*  创建日期:2021-12-22
* @return java.lang.String
*/
public String getVsourcemocode() {
return this.vsourcemocode;
} 

/**
* 属性vsourcemocode的Setter方法.属性名：生产订单号
* 创建日期:2021-12-22
* @param newVsourcemocode java.lang.String
*/
public void setVsourcemocode ( String vsourcemocode) {
this.vsourcemocode=vsourcemocode;
} 
 
/**
* 属性 vsourcemoid的Getter方法.属性名：生产订单主键
*  创建日期:2021-12-22
* @return java.lang.String
*/
public String getVsourcemoid() {
return this.vsourcemoid;
} 

/**
* 属性vsourcemoid的Setter方法.属性名：生产订单主键
* 创建日期:2021-12-22
* @param newVsourcemoid java.lang.String
*/
public void setVsourcemoid ( String vsourcemoid) {
this.vsourcemoid=vsourcemoid;
} 
 
/**
* 属性 vsourcemorowid的Getter方法.属性名：生产订单明细主键
*  创建日期:2021-12-22
* @return java.lang.String
*/
public String getVsourcemorowid() {
return this.vsourcemorowid;
} 

/**
* 属性vsourcemorowid的Setter方法.属性名：生产订单明细主键
* 创建日期:2021-12-22
* @param newVsourcemorowid java.lang.String
*/
public void setVsourcemorowid ( String vsourcemorowid) {
this.vsourcemorowid=vsourcemorowid;
} 
 
/**
* 属性 pcmaterialvid的Getter方法.属性名：产品编码
*  创建日期:2021-12-22
* @return nc.vo.bd.material.MaterialVO
*/
public String getPcmaterialvid() {
return this.pcmaterialvid;
} 

/**
* 属性pcmaterialvid的Setter方法.属性名：产品编码
* 创建日期:2021-12-22
* @param newPcmaterialvid nc.vo.bd.material.MaterialVO
*/
public void setPcmaterialvid ( String pcmaterialvid) {
this.pcmaterialvid=pcmaterialvid;
} 
 
/**
* 属性 pk_pickm的Getter方法.属性名：挪入备料计划
*  创建日期:2021-12-22
* @return java.lang.String
*/
public String getPk_pickm() {
return this.pk_pickm;
} 

/**
* 属性pk_pickm的Setter方法.属性名：挪入备料计划
* 创建日期:2021-12-22
* @param newPk_pickm java.lang.String
*/
public void setPk_pickm ( String pk_pickm) {
this.pk_pickm=pk_pickm;
} 
 
/**
* 属性 in_dept的Getter方法.属性名：挪入部门
*  创建日期:2021-12-22
* @return nc.vo.org.DeptVO
*/
public String getIn_dept() {
return this.in_dept;
} 

/**
* 属性in_dept的Setter方法.属性名：挪入部门
* 创建日期:2021-12-22
* @param newIn_dept nc.vo.org.DeptVO
*/
public void setIn_dept ( String in_dept) {
this.in_dept=in_dept;
} 
 
/**
* 属性 生成时间戳的Getter方法.属性名：时间戳
*  创建日期:2021-12-22
* @return nc.vo.pub.lang.UFDateTime
*/
public UFDateTime getTs() {
return this.ts;
}
/**
* 属性生成时间戳的Setter方法.属性名：时间戳
* 创建日期:2021-12-22
* @param newts nc.vo.pub.lang.UFDateTime
*/
public void setTs(UFDateTime ts){
this.ts=ts;
} 
     
    @Override
    public IVOMeta getMetaData() {
    return VOMetaFactory.getInstance().getVOMeta("uapbd.TranMaterialVO");
    }
   }
    