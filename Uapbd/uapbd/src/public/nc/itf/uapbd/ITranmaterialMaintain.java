package nc.itf.uapbd;

import nc.ui.querytemplate.querytree.IQueryScheme;
import nc.vo.uapbd.tranmaterial.AggTranMaterialVO;
import nc.vo.uapbd.tranmaterial.TranMaterialVO;
import nc.vo.pub.BusinessException;

public interface ITranmaterialMaintain {

	public void delete(AggTranMaterialVO[] clientFullVOs,
			AggTranMaterialVO[] originBills) throws BusinessException;

	public AggTranMaterialVO[] insert(AggTranMaterialVO[] clientFullVOs,
			AggTranMaterialVO[] originBills) throws BusinessException;

	public AggTranMaterialVO[] update(AggTranMaterialVO[] clientFullVOs,
			AggTranMaterialVO[] originBills) throws BusinessException;

	public AggTranMaterialVO[] query(IQueryScheme queryScheme)
			throws BusinessException;

	public AggTranMaterialVO[] save(AggTranMaterialVO[] clientFullVOs,
			AggTranMaterialVO[] originBills) throws BusinessException;

	public AggTranMaterialVO[] unsave(AggTranMaterialVO[] clientFullVOs,
			AggTranMaterialVO[] originBills) throws BusinessException;

	public AggTranMaterialVO[] approve(AggTranMaterialVO[] clientFullVOs,
			AggTranMaterialVO[] originBills) throws BusinessException;

	public AggTranMaterialVO[] unapprove(AggTranMaterialVO[] clientFullVOs,
			AggTranMaterialVO[] originBills) throws BusinessException;
}
