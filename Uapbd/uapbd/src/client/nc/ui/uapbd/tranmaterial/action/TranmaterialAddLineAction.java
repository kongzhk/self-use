package nc.ui.uapbd.tranmaterial.action;

import nc.ui.pubapp.uif2app.actions.batch.BatchAddLineAction;
import nc.vo.uapbd.tranmaterial.TranMaterialVO;
/**
  batch addLine or insLine action autogen
*/
public class TranmaterialAddLineAction extends BatchAddLineAction {

	private static final long serialVersionUID = 1L;

	@Override
	protected void setDefaultData(Object obj) {
		super.setDefaultData(obj);
		TranMaterialVO singleDocVO = (TranMaterialVO) obj;
		singleDocVO.setPk_group(this.getModel().getContext().getPk_group());
		singleDocVO.setPk_org(this.getModel().getContext().getPk_org());
	}

}