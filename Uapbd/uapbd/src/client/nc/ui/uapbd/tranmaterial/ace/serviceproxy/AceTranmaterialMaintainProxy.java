package nc.ui.uapbd.tranmaterial.ace.serviceproxy;

import nc.bs.framework.common.NCLocator;
import nc.itf.uapbd.ITranmaterialMaintain;
import nc.ui.pubapp.uif2app.query2.model.IQueryService;
import nc.ui.querytemplate.querytree.IQueryScheme;

/**
 * 示例单据的操作代理
 * 
 * @author author
 * @version tempProject version
 */
public class AceTranmaterialMaintainProxy implements IQueryService {
	@Override
	public Object[] queryByQueryScheme(IQueryScheme queryScheme)
			throws Exception {
		ITranmaterialMaintain query = NCLocator.getInstance().lookup(
				ITranmaterialMaintain.class);
		return query.query(queryScheme);
	}

}