package nc.impl.pub.ace;

import nc.bs.uapbd.tranmaterial.ace.bp.AceTranmaterialInsertBP;
import nc.bs.uapbd.tranmaterial.ace.bp.AceTranmaterialUpdateBP;
import nc.bs.uapbd.tranmaterial.ace.bp.AceTranmaterialDeleteBP;
import nc.bs.uapbd.tranmaterial.ace.bp.AceTranmaterialSendApproveBP;
import nc.bs.uapbd.tranmaterial.ace.bp.AceTranmaterialUnSendApproveBP;
import nc.bs.uapbd.tranmaterial.ace.bp.AceTranmaterialApproveBP;
import nc.bs.uapbd.tranmaterial.ace.bp.AceTranmaterialUnApproveBP;
import nc.impl.pubapp.pattern.data.bill.BillLazyQuery;
import nc.impl.pubapp.pattern.data.bill.tool.BillTransferTool;
import nc.ui.querytemplate.querytree.IQueryScheme;
import nc.vo.uapbd.tranmaterial.AggTranMaterialVO;
import nc.vo.pub.BusinessException;
import nc.vo.pub.VOStatus;
import nc.vo.pubapp.pattern.exception.ExceptionUtils;

public abstract class AceTranmaterialPubServiceImpl {
	// 新增
	public AggTranMaterialVO[] pubinsertBills(AggTranMaterialVO[] clientFullVOs,
			AggTranMaterialVO[] originBills) throws BusinessException {
		try {
			// 数据库中数据和前台传递过来的差异VO合并后的结果
			BillTransferTool<AggTranMaterialVO> transferTool = new BillTransferTool<AggTranMaterialVO>(
					clientFullVOs);
			// 调用BP
			AceTranmaterialInsertBP action = new AceTranmaterialInsertBP();
			AggTranMaterialVO[] retvos = action.insert(clientFullVOs);
			// 构造返回数据
			return transferTool.getBillForToClient(retvos);
		} catch (Exception e) {
			ExceptionUtils.marsh(e);
		}
		return null;
	}

	// 删除
	public void pubdeleteBills(AggTranMaterialVO[] clientFullVOs,
			AggTranMaterialVO[] originBills) throws BusinessException {
		try {
			// 调用BP
			new AceTranmaterialDeleteBP().delete(clientFullVOs);
		} catch (Exception e) {
			ExceptionUtils.marsh(e);
		}
	}

	// 修改
	public AggTranMaterialVO[] pubupdateBills(AggTranMaterialVO[] clientFullVOs,
			AggTranMaterialVO[] originBills) throws BusinessException {
		try {
			// 加锁 + 检查ts
			BillTransferTool<AggTranMaterialVO> transferTool = new BillTransferTool<AggTranMaterialVO>(
					clientFullVOs);
			AceTranmaterialUpdateBP bp = new AceTranmaterialUpdateBP();
			AggTranMaterialVO[] retvos = bp.update(clientFullVOs, originBills);
			// 构造返回数据
			return transferTool.getBillForToClient(retvos);
		} catch (Exception e) {
			ExceptionUtils.marsh(e);
		}
		return null;
	}

	public AggTranMaterialVO[] pubquerybills(IQueryScheme queryScheme)
			throws BusinessException {
		AggTranMaterialVO[] bills = null;
		try {
			this.preQuery(queryScheme);
			BillLazyQuery<AggTranMaterialVO> query = new BillLazyQuery<AggTranMaterialVO>(
					AggTranMaterialVO.class);
			bills = query.query(queryScheme, null);
		} catch (Exception e) {
			ExceptionUtils.marsh(e);
		}
		return bills;
	}

	/**
	 * 由子类实现，查询之前对queryScheme进行加工，加入自己的逻辑
	 * 
	 * @param queryScheme
	 */
	protected void preQuery(IQueryScheme queryScheme) {
		// 查询之前对queryScheme进行加工，加入自己的逻辑
	}

	// 提交
	public AggTranMaterialVO[] pubsendapprovebills(
			AggTranMaterialVO[] clientFullVOs, AggTranMaterialVO[] originBills)
			throws BusinessException {
		AceTranmaterialSendApproveBP bp = new AceTranmaterialSendApproveBP();
		AggTranMaterialVO[] retvos = bp.sendApprove(clientFullVOs, originBills);
		return retvos;
	}

	// 收回
	public AggTranMaterialVO[] pubunsendapprovebills(
			AggTranMaterialVO[] clientFullVOs, AggTranMaterialVO[] originBills)
			throws BusinessException {
		AceTranmaterialUnSendApproveBP bp = new AceTranmaterialUnSendApproveBP();
		AggTranMaterialVO[] retvos = bp.unSend(clientFullVOs, originBills);
		return retvos;
	};

	// 审批
	public AggTranMaterialVO[] pubapprovebills(AggTranMaterialVO[] clientFullVOs,
			AggTranMaterialVO[] originBills) throws BusinessException {
		for (int i = 0; clientFullVOs != null && i < clientFullVOs.length; i++) {
			clientFullVOs[i].getParentVO().setStatus(VOStatus.UPDATED);
		}
		AceTranmaterialApproveBP bp = new AceTranmaterialApproveBP();
		AggTranMaterialVO[] retvos = bp.approve(clientFullVOs, originBills);
		return retvos;
	}

	// 弃审

	public AggTranMaterialVO[] pubunapprovebills(AggTranMaterialVO[] clientFullVOs,
			AggTranMaterialVO[] originBills) throws BusinessException {
		for (int i = 0; clientFullVOs != null && i < clientFullVOs.length; i++) {
			clientFullVOs[i].getParentVO().setStatus(VOStatus.UPDATED);
		}
		AceTranmaterialUnApproveBP bp = new AceTranmaterialUnApproveBP();
		AggTranMaterialVO[] retvos = bp.unApprove(clientFullVOs, originBills);
		return retvos;
	}

}