package nc.impl.uapbd;

import nc.impl.pub.ace.AceTranmaterialPubServiceImpl;
import nc.ui.querytemplate.querytree.IQueryScheme;
import nc.vo.uapbd.tranmaterial.AggTranMaterialVO;
import nc.itf.uapbd.ITranmaterialMaintain;
import nc.vo.pub.BusinessException;

public class TranmaterialMaintainImpl extends AceTranmaterialPubServiceImpl
		implements ITranmaterialMaintain {

	@Override
	public void delete(AggTranMaterialVO[] clientFullVOs,
			AggTranMaterialVO[] originBills) throws BusinessException {
		super.pubdeleteBills(clientFullVOs, originBills);
	}

	@Override
	public AggTranMaterialVO[] insert(AggTranMaterialVO[] clientFullVOs,
			AggTranMaterialVO[] originBills) throws BusinessException {
		return super.pubinsertBills(clientFullVOs, originBills);
	}

	@Override
	public AggTranMaterialVO[] update(AggTranMaterialVO[] clientFullVOs,
			AggTranMaterialVO[] originBills) throws BusinessException {
		return super.pubupdateBills(clientFullVOs, originBills);
	}

	@Override
	public AggTranMaterialVO[] query(IQueryScheme queryScheme)
			throws BusinessException {
		return super.pubquerybills(queryScheme);
	}

	@Override
	public AggTranMaterialVO[] save(AggTranMaterialVO[] clientFullVOs,
			AggTranMaterialVO[] originBills) throws BusinessException {
		return super.pubsendapprovebills(clientFullVOs, originBills);
	}

	@Override
	public AggTranMaterialVO[] unsave(AggTranMaterialVO[] clientFullVOs,
			AggTranMaterialVO[] originBills) throws BusinessException {
		return super.pubunsendapprovebills(clientFullVOs, originBills);
	}

	@Override
	public AggTranMaterialVO[] approve(AggTranMaterialVO[] clientFullVOs,
			AggTranMaterialVO[] originBills) throws BusinessException {
		return super.pubapprovebills(clientFullVOs, originBills);
	}

	@Override
	public AggTranMaterialVO[] unapprove(AggTranMaterialVO[] clientFullVOs,
			AggTranMaterialVO[] originBills) throws BusinessException {
		return super.pubunapprovebills(clientFullVOs, originBills);
	}

}
