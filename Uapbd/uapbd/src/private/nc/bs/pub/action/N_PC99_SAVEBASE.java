package nc.bs.pub.action;

import nc.bs.framework.common.NCLocator;
import nc.bs.pubapp.pf.action.AbstractPfAction;
import nc.impl.pubapp.pattern.rule.IRule;
import nc.impl.pubapp.pattern.rule.processer.CompareAroundProcesser;
import nc.vo.jcom.lang.StringUtil;
import nc.vo.pub.BusinessException;
import nc.vo.pubapp.pattern.exception.ExceptionUtils;

import nc.bs.uapbd.tranmaterial.plugin.bpplugin.TranmaterialPluginPoint;
import nc.vo.uapbd.tranmaterial.AggTranMaterialVO;
import nc.itf.uapbd.ITranmaterialMaintain;

public class N_PC99_SAVEBASE extends AbstractPfAction<AggTranMaterialVO> {

	@Override
	protected CompareAroundProcesser<AggTranMaterialVO> getCompareAroundProcesserWithRules(
			Object userObj) {
		CompareAroundProcesser<AggTranMaterialVO> processor = null;
		AggTranMaterialVO[] clientFullVOs = (AggTranMaterialVO[]) this.getVos();
		if (!StringUtil.isEmptyWithTrim(clientFullVOs[0].getParentVO()
				.getPrimaryKey())) {
			processor = new CompareAroundProcesser<AggTranMaterialVO>(
					TranmaterialPluginPoint.SCRIPT_UPDATE);
		} else {
			processor = new CompareAroundProcesser<AggTranMaterialVO>(
					TranmaterialPluginPoint.SCRIPT_INSERT);
		}
		// TODO 在此处添加前后规则
		IRule<AggTranMaterialVO> rule = null;

		return processor;
	}

	@Override
	protected AggTranMaterialVO[] processBP(Object userObj,
			AggTranMaterialVO[] clientFullVOs, AggTranMaterialVO[] originBills) {

		AggTranMaterialVO[] bills = null;
		try {
			ITranmaterialMaintain operator = NCLocator.getInstance()
					.lookup(ITranmaterialMaintain.class);
			if (!StringUtil.isEmptyWithTrim(clientFullVOs[0].getParentVO()
					.getPrimaryKey())) {
				bills = operator.update(clientFullVOs, originBills);
			} else {
				bills = operator.insert(clientFullVOs, originBills);
			}
		} catch (BusinessException e) {
			ExceptionUtils.wrappBusinessException(e.getMessage());
		}
		return bills;
	}
}
