package nc.bs.pub.action;

import nc.bs.framework.common.NCLocator;
import nc.bs.pubapp.pf.action.AbstractPfAction;
import nc.impl.pubapp.pattern.rule.processer.CompareAroundProcesser;
import nc.vo.pub.BusinessException;
import nc.vo.pubapp.pattern.exception.ExceptionUtils;
import nc.bs.uapbd.tranmaterial.plugin.bpplugin.TranmaterialPluginPoint;
import nc.vo.uapbd.tranmaterial.AggTranMaterialVO;
import nc.vo.uapbd.tranmaterial.TranMaterialVO;
import nc.itf.uapbd.ITranmaterialMaintain;

public class N_PC99_DELETE extends AbstractPfAction<AggTranMaterialVO> {

	@Override
	protected CompareAroundProcesser<AggTranMaterialVO> getCompareAroundProcesserWithRules(
			Object userObj) {
		CompareAroundProcesser<AggTranMaterialVO> processor = new CompareAroundProcesser<AggTranMaterialVO>(
				TranmaterialPluginPoint.SCRIPT_DELETE);
		// TODO 在此处添加前后规则
		return processor;
	}

	@Override
	protected AggTranMaterialVO[] processBP(Object userObj,
			AggTranMaterialVO[] clientFullVOs, AggTranMaterialVO[] originBills) {
		ITranmaterialMaintain operator = NCLocator.getInstance().lookup(
				ITranmaterialMaintain.class);
		try {
			operator.delete(clientFullVOs, originBills);
		} catch (BusinessException e) {
			ExceptionUtils.wrappBusinessException(e.getMessage());
		}
		return clientFullVOs;
	}

}
