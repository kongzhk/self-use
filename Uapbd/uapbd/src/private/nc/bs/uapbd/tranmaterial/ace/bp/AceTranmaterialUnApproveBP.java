package nc.bs.uapbd.tranmaterial.ace.bp;

import nc.impl.pubapp.pattern.data.bill.BillUpdate;
import nc.vo.uapbd.tranmaterial.AggTranMaterialVO;
import nc.vo.pub.VOStatus;

/**
 * 标准单据弃审的BP
 */
public class AceTranmaterialUnApproveBP {

	public AggTranMaterialVO[] unApprove(AggTranMaterialVO[] clientBills,
			AggTranMaterialVO[] originBills) {
		for (AggTranMaterialVO clientBill : clientBills) {
			clientBill.getParentVO().setStatus(VOStatus.UPDATED);
		}
		BillUpdate<AggTranMaterialVO> update = new BillUpdate<AggTranMaterialVO>();
		AggTranMaterialVO[] returnVos = update.update(clientBills, originBills);
		return returnVos;
	}
}
