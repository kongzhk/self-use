package nc.bs.uapbd.tranmaterial.ace.bp;

import nc.impl.pubapp.pattern.data.bill.BillUpdate;
import nc.vo.pub.VOStatus;
import nc.vo.uapbd.tranmaterial.AggTranMaterialVO;

/**
 * 标准单据审核的BP
 */
public class AceTranmaterialApproveBP {

	/**
	 * 审核动作
	 * 
	 * @param vos
	 * @param script
	 * @return
	 */
	public AggTranMaterialVO[] approve(AggTranMaterialVO[] clientBills,
			AggTranMaterialVO[] originBills) {
		for (AggTranMaterialVO clientBill : clientBills) {
			clientBill.getParentVO().setStatus(VOStatus.UPDATED);
		}
		BillUpdate<AggTranMaterialVO> update = new BillUpdate<AggTranMaterialVO>();
		AggTranMaterialVO[] returnVos = update.update(clientBills, originBills);
		return returnVos;
	}

}
