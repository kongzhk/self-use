package nc.bs.uapbd.tranmaterial.ace.bp;

import nc.impl.pubapp.pattern.data.bill.BillUpdate;
import nc.vo.uapbd.tranmaterial.AggTranMaterialVO;
import nc.vo.pub.VOStatus;
import nc.vo.pub.pf.BillStatusEnum;

/**
 * 标准单据收回的BP
 */
public class AceTranmaterialUnSendApproveBP {

	public AggTranMaterialVO[] unSend(AggTranMaterialVO[] clientBills,
			AggTranMaterialVO[] originBills) {
		// 把VO持久化到数据库中
		this.setHeadVOStatus(clientBills);
		BillUpdate<AggTranMaterialVO> update = new BillUpdate<AggTranMaterialVO>();
		AggTranMaterialVO[] returnVos = update.update(clientBills, originBills);
		return returnVos;
	}

	private void setHeadVOStatus(AggTranMaterialVO[] clientBills) {
		for (AggTranMaterialVO clientBill : clientBills) {
			clientBill.getParentVO().setAttributeValue("${vmObject.billstatus}",
					BillStatusEnum.FREE.value());
			clientBill.getParentVO().setStatus(VOStatus.UPDATED);
		}
	}
}
