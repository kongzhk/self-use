package nc.bs.uapbd.tranmaterial.ace.bp;

import nc.impl.pubapp.pattern.data.vo.SchemeVOQuery;
import nc.ui.querytemplate.querytree.IQueryScheme;
import nc.vo.pubapp.query2.sql.process.QuerySchemeProcessor;
import nc.vo.uapbd.tranmaterial.TranMaterialVO;

public class AceTranmaterialBP {

	public TranMaterialVO[] queryByQueryScheme(IQueryScheme querySheme) {
		QuerySchemeProcessor p = new QuerySchemeProcessor(querySheme);
		p.appendFuncPermissionOrgSql();
		return new SchemeVOQuery<TranMaterialVO>(TranMaterialVO.class).query(querySheme,
				null);
	}
}
