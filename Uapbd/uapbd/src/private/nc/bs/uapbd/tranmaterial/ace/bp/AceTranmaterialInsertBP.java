package nc.bs.uapbd.tranmaterial.ace.bp;

import nc.bs.uapbd.tranmaterial.plugin.bpplugin.TranmaterialPluginPoint;
import nc.impl.pubapp.pattern.data.bill.template.InsertBPTemplate;
import nc.impl.pubapp.pattern.rule.processer.AroundProcesser;
import nc.impl.pubapp.pattern.rule.IRule;
import nc.vo.uapbd.tranmaterial.AggTranMaterialVO;

/**
 * 标准单据新增BP
 */
public class AceTranmaterialInsertBP {

	public AggTranMaterialVO[] insert(AggTranMaterialVO[] bills) {

		InsertBPTemplate<AggTranMaterialVO> bp = new InsertBPTemplate<AggTranMaterialVO>(
				TranmaterialPluginPoint.INSERT);
		this.addBeforeRule(bp.getAroundProcesser());
		this.addAfterRule(bp.getAroundProcesser());
		return bp.insert(bills);

	}

	/**
	 * 新增后规则
	 * 
	 * @param processor
	 */
	private void addAfterRule(AroundProcesser<AggTranMaterialVO> processor) {
		// TODO 新增后规则
		IRule<AggTranMaterialVO> rule = null;
	}

	/**
	 * 新增前规则
	 * 
	 * @param processor
	 */
	private void addBeforeRule(AroundProcesser<AggTranMaterialVO> processer) {
		// TODO 新增前规则
		IRule<AggTranMaterialVO> rule = null;
		rule = new nc.bs.pubapp.pub.rule.FillInsertDataRule();
		processer.addBeforeRule(rule);
	}
}
