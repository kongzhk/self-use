package nc.vo.uapbd.tranm;

import nc.vo.pubapp.pattern.model.meta.entity.bill.AbstractBillMeta;

public class AggTransMaterialVOMeta extends AbstractBillMeta{
	
	public AggTransMaterialVOMeta(){
		this.init();
	}
	
	private void init() {
		this.setParent(nc.vo.uapbd.tranm.TransMaterialVO.class);
	}
}