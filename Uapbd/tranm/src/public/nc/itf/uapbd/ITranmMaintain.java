package nc.itf.uapbd;

import nc.ui.querytemplate.querytree.IQueryScheme;
import nc.vo.uapbd.tranm.AggTransMaterialVO;
import nc.vo.pub.BusinessException;

public interface ITranmMaintain {

	public void delete(AggTransMaterialVO[] clientFullVOs,
			AggTransMaterialVO[] originBills) throws BusinessException;

	public AggTransMaterialVO[] insert(AggTransMaterialVO[] clientFullVOs,
			AggTransMaterialVO[] originBills) throws BusinessException;

	public AggTransMaterialVO[] update(AggTransMaterialVO[] clientFullVOs,
			AggTransMaterialVO[] originBills) throws BusinessException;

	public AggTransMaterialVO[] query(IQueryScheme queryScheme)
			throws BusinessException;

	public AggTransMaterialVO[] save(AggTransMaterialVO[] clientFullVOs,
			AggTransMaterialVO[] originBills) throws BusinessException;

	public AggTransMaterialVO[] unsave(AggTransMaterialVO[] clientFullVOs,
			AggTransMaterialVO[] originBills) throws BusinessException;

	public AggTransMaterialVO[] approve(AggTransMaterialVO[] clientFullVOs,
			AggTransMaterialVO[] originBills) throws BusinessException;

	public AggTransMaterialVO[] unapprove(AggTransMaterialVO[] clientFullVOs,
			AggTransMaterialVO[] originBills) throws BusinessException;
}
