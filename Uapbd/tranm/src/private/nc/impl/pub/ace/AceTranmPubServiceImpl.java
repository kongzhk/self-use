package nc.impl.pub.ace;

import nc.bs.uapbd.tranm.ace.bp.AceTranmInsertBP;
import nc.bs.uapbd.tranm.ace.bp.AceTranmUpdateBP;
import nc.bs.uapbd.tranm.ace.bp.AceTranmDeleteBP;
import nc.bs.uapbd.tranm.ace.bp.AceTranmSendApproveBP;
import nc.bs.uapbd.tranm.ace.bp.AceTranmUnSendApproveBP;
import nc.bs.uapbd.tranm.ace.bp.AceTranmApproveBP;
import nc.bs.uapbd.tranm.ace.bp.AceTranmUnApproveBP;
import nc.impl.pubapp.pattern.data.bill.BillLazyQuery;
import nc.impl.pubapp.pattern.data.bill.tool.BillTransferTool;
import nc.ui.querytemplate.querytree.IQueryScheme;
import nc.vo.uapbd.tranm.AggTransMaterialVO;
import nc.vo.pub.BusinessException;
import nc.vo.pub.VOStatus;
import nc.vo.pubapp.pattern.exception.ExceptionUtils;

public abstract class AceTranmPubServiceImpl {
	// 新增
	public AggTransMaterialVO[] pubinsertBills(AggTransMaterialVO[] clientFullVOs,
			AggTransMaterialVO[] originBills) throws BusinessException {
		try {
			// 数据库中数据和前台传递过来的差异VO合并后的结果
			BillTransferTool<AggTransMaterialVO> transferTool = new BillTransferTool<AggTransMaterialVO>(
					clientFullVOs);
			// 调用BP
			AceTranmInsertBP action = new AceTranmInsertBP();
			AggTransMaterialVO[] retvos = action.insert(clientFullVOs);
			// 构造返回数据
			return transferTool.getBillForToClient(retvos);
		} catch (Exception e) {
			ExceptionUtils.marsh(e);
		}
		return null;
	}

	// 删除
	public void pubdeleteBills(AggTransMaterialVO[] clientFullVOs,
			AggTransMaterialVO[] originBills) throws BusinessException {
		try {
			// 调用BP
			new AceTranmDeleteBP().delete(clientFullVOs);
		} catch (Exception e) {
			ExceptionUtils.marsh(e);
		}
	}

	// 修改
	public AggTransMaterialVO[] pubupdateBills(AggTransMaterialVO[] clientFullVOs,
			AggTransMaterialVO[] originBills) throws BusinessException {
		try {
			// 加锁 + 检查ts
			BillTransferTool<AggTransMaterialVO> transferTool = new BillTransferTool<AggTransMaterialVO>(
					clientFullVOs);
			AceTranmUpdateBP bp = new AceTranmUpdateBP();
			AggTransMaterialVO[] retvos = bp.update(clientFullVOs, originBills);
			// 构造返回数据
			return transferTool.getBillForToClient(retvos);
		} catch (Exception e) {
			ExceptionUtils.marsh(e);
		}
		return null;
	}

	public AggTransMaterialVO[] pubquerybills(IQueryScheme queryScheme)
			throws BusinessException {
		AggTransMaterialVO[] bills = null;
		try {
			this.preQuery(queryScheme);
			BillLazyQuery<AggTransMaterialVO> query = new BillLazyQuery<AggTransMaterialVO>(
					AggTransMaterialVO.class);
			bills = query.query(queryScheme, null);
		} catch (Exception e) {
			ExceptionUtils.marsh(e);
		}
		return bills;
	}

	/**
	 * 由子类实现，查询之前对queryScheme进行加工，加入自己的逻辑
	 * 
	 * @param queryScheme
	 */
	protected void preQuery(IQueryScheme queryScheme) {
		// 查询之前对queryScheme进行加工，加入自己的逻辑
	}

	// 提交
	public AggTransMaterialVO[] pubsendapprovebills(
			AggTransMaterialVO[] clientFullVOs, AggTransMaterialVO[] originBills)
			throws BusinessException {
		AceTranmSendApproveBP bp = new AceTranmSendApproveBP();
		AggTransMaterialVO[] retvos = bp.sendApprove(clientFullVOs, originBills);
		return retvos;
	}

	// 收回
	public AggTransMaterialVO[] pubunsendapprovebills(
			AggTransMaterialVO[] clientFullVOs, AggTransMaterialVO[] originBills)
			throws BusinessException {
		AceTranmUnSendApproveBP bp = new AceTranmUnSendApproveBP();
		AggTransMaterialVO[] retvos = bp.unSend(clientFullVOs, originBills);
		return retvos;
	};

	// 审批
	public AggTransMaterialVO[] pubapprovebills(AggTransMaterialVO[] clientFullVOs,
			AggTransMaterialVO[] originBills) throws BusinessException {
		for (int i = 0; clientFullVOs != null && i < clientFullVOs.length; i++) {
			clientFullVOs[i].getParentVO().setStatus(VOStatus.UPDATED);
		}
		AceTranmApproveBP bp = new AceTranmApproveBP();
		AggTransMaterialVO[] retvos = bp.approve(clientFullVOs, originBills);
		return retvos;
	}

	// 弃审

	public AggTransMaterialVO[] pubunapprovebills(AggTransMaterialVO[] clientFullVOs,
			AggTransMaterialVO[] originBills) throws BusinessException {
		for (int i = 0; clientFullVOs != null && i < clientFullVOs.length; i++) {
			clientFullVOs[i].getParentVO().setStatus(VOStatus.UPDATED);
		}
		AceTranmUnApproveBP bp = new AceTranmUnApproveBP();
		AggTransMaterialVO[] retvos = bp.unApprove(clientFullVOs, originBills);
		return retvos;
	}

}