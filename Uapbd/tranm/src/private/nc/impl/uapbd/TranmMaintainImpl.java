package nc.impl.uapbd;

import nc.impl.pub.ace.AceTranmPubServiceImpl;
import nc.ui.querytemplate.querytree.IQueryScheme;
import nc.vo.uapbd.tranm.AggTransMaterialVO;
import nc.itf.uapbd.ITranmMaintain;
import nc.vo.pub.BusinessException;

public class TranmMaintainImpl extends AceTranmPubServiceImpl
		implements ITranmMaintain {

	@Override
	public void delete(AggTransMaterialVO[] clientFullVOs,
			AggTransMaterialVO[] originBills) throws BusinessException {
		super.pubdeleteBills(clientFullVOs, originBills);
	}

	@Override
	public AggTransMaterialVO[] insert(AggTransMaterialVO[] clientFullVOs,
			AggTransMaterialVO[] originBills) throws BusinessException {
		return super.pubinsertBills(clientFullVOs, originBills);
	}

	@Override
	public AggTransMaterialVO[] update(AggTransMaterialVO[] clientFullVOs,
			AggTransMaterialVO[] originBills) throws BusinessException {
		return super.pubupdateBills(clientFullVOs, originBills);
	}

	@Override
	public AggTransMaterialVO[] query(IQueryScheme queryScheme)
			throws BusinessException {
		return super.pubquerybills(queryScheme);
	}

	@Override
	public AggTransMaterialVO[] save(AggTransMaterialVO[] clientFullVOs,
			AggTransMaterialVO[] originBills) throws BusinessException {
		return super.pubsendapprovebills(clientFullVOs, originBills);
	}

	@Override
	public AggTransMaterialVO[] unsave(AggTransMaterialVO[] clientFullVOs,
			AggTransMaterialVO[] originBills) throws BusinessException {
		return super.pubunsendapprovebills(clientFullVOs, originBills);
	}

	@Override
	public AggTransMaterialVO[] approve(AggTransMaterialVO[] clientFullVOs,
			AggTransMaterialVO[] originBills) throws BusinessException {
		return super.pubapprovebills(clientFullVOs, originBills);
	}

	@Override
	public AggTransMaterialVO[] unapprove(AggTransMaterialVO[] clientFullVOs,
			AggTransMaterialVO[] originBills) throws BusinessException {
		return super.pubunapprovebills(clientFullVOs, originBills);
	}

}
