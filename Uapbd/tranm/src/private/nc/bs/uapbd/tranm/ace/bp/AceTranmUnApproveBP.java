package nc.bs.uapbd.tranm.ace.bp;

import nc.impl.pubapp.pattern.data.bill.BillUpdate;
import nc.vo.uapbd.tranm.AggTransMaterialVO;
import nc.vo.pub.VOStatus;

/**
 * 标准单据弃审的BP
 */
public class AceTranmUnApproveBP {

	public AggTransMaterialVO[] unApprove(AggTransMaterialVO[] clientBills,
			AggTransMaterialVO[] originBills) {
		for (AggTransMaterialVO clientBill : clientBills) {
			clientBill.getParentVO().setStatus(VOStatus.UPDATED);
		}
		BillUpdate<AggTransMaterialVO> update = new BillUpdate<AggTransMaterialVO>();
		AggTransMaterialVO[] returnVos = update.update(clientBills, originBills);
		return returnVos;
	}
}
