package nc.bs.uapbd.tranm.ace.bp;

import nc.bs.uapbd.tranm.plugin.bpplugin.TranmPluginPoint;
import nc.impl.pubapp.pattern.data.bill.template.InsertBPTemplate;
import nc.impl.pubapp.pattern.rule.processer.AroundProcesser;
import nc.impl.pubapp.pattern.rule.IRule;
import nc.vo.uapbd.tranm.AggTransMaterialVO;

/**
 * 标准单据新增BP
 */
public class AceTranmInsertBP {

	public AggTransMaterialVO[] insert(AggTransMaterialVO[] bills) {

		InsertBPTemplate<AggTransMaterialVO> bp = new InsertBPTemplate<AggTransMaterialVO>(
				TranmPluginPoint.INSERT);
		this.addBeforeRule(bp.getAroundProcesser());
		this.addAfterRule(bp.getAroundProcesser());
		return bp.insert(bills);

	}

	/**
	 * 新增后规则
	 * 
	 * @param processor
	 */
	private void addAfterRule(AroundProcesser<AggTransMaterialVO> processor) {
		// TODO 新增后规则
		IRule<AggTransMaterialVO> rule = null;
	}

	/**
	 * 新增前规则
	 * 
	 * @param processor
	 */
	private void addBeforeRule(AroundProcesser<AggTransMaterialVO> processer) {
		// TODO 新增前规则
		IRule<AggTransMaterialVO> rule = null;
		rule = new nc.bs.pubapp.pub.rule.FillInsertDataRule();
		processer.addBeforeRule(rule);
	}
}
