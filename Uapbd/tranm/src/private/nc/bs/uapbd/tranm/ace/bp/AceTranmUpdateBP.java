package nc.bs.uapbd.tranm.ace.bp;

import nc.bs.uapbd.tranm.plugin.bpplugin.TranmPluginPoint;
import nc.impl.pubapp.pattern.data.bill.template.UpdateBPTemplate;
import nc.impl.pubapp.pattern.rule.processer.CompareAroundProcesser;
import nc.impl.pubapp.pattern.rule.IRule;
import nc.vo.uapbd.tranm.AggTransMaterialVO;

/**
 * 修改保存的BP
 * 
 */
public class AceTranmUpdateBP {

	public AggTransMaterialVO[] update(AggTransMaterialVO[] bills,
			AggTransMaterialVO[] originBills) {
		// 调用修改模板
		UpdateBPTemplate<AggTransMaterialVO> bp = new UpdateBPTemplate<AggTransMaterialVO>(
				TranmPluginPoint.UPDATE);
		// 执行前规则
		this.addBeforeRule(bp.getAroundProcesser());
		// 执行后规则
		this.addAfterRule(bp.getAroundProcesser());
		return bp.update(bills, originBills);
	}

	private void addAfterRule(CompareAroundProcesser<AggTransMaterialVO> processer) {
		// TODO 后规则
		IRule<AggTransMaterialVO> rule = null;

	}

	private void addBeforeRule(CompareAroundProcesser<AggTransMaterialVO> processer) {
		// TODO 前规则
		IRule<AggTransMaterialVO> rule = null;
		rule = new nc.bs.pubapp.pub.rule.FillUpdateDataRule();
		processer.addBeforeRule(rule);
	}

}
