package nc.bs.uapbd.tranm.ace.bp;

import nc.impl.pubapp.pattern.data.bill.BillUpdate;
import nc.vo.uapbd.tranm.AggTransMaterialVO;
import nc.vo.pub.VOStatus;
import nc.vo.pub.pf.BillStatusEnum;

/**
 * 标准单据收回的BP
 */
public class AceTranmUnSendApproveBP {

	public AggTransMaterialVO[] unSend(AggTransMaterialVO[] clientBills,
			AggTransMaterialVO[] originBills) {
		// 把VO持久化到数据库中
		this.setHeadVOStatus(clientBills);
		BillUpdate<AggTransMaterialVO> update = new BillUpdate<AggTransMaterialVO>();
		AggTransMaterialVO[] returnVos = update.update(clientBills, originBills);
		return returnVos;
	}

	private void setHeadVOStatus(AggTransMaterialVO[] clientBills) {
		for (AggTransMaterialVO clientBill : clientBills) {
			clientBill.getParentVO().setAttributeValue("${vmObject.billstatus}",
					BillStatusEnum.FREE.value());
			clientBill.getParentVO().setStatus(VOStatus.UPDATED);
		}
	}
}
