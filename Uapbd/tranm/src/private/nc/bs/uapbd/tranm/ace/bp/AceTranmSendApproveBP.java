package nc.bs.uapbd.tranm.ace.bp;

import nc.impl.pubapp.pattern.data.bill.BillUpdate;
import nc.vo.uapbd.tranm.AggTransMaterialVO;
import nc.vo.pub.VOStatus;
import nc.vo.pub.pf.BillStatusEnum;

/**
 * 标准单据送审的BP
 */
public class AceTranmSendApproveBP {
	/**
	 * 送审动作
	 * 
	 * @param vos
	 *            单据VO数组
	 * @param script
	 *            单据动作脚本对象
	 * @return 送审后的单据VO数组
	 */

	public AggTransMaterialVO[] sendApprove(AggTransMaterialVO[] clientBills,
			AggTransMaterialVO[] originBills) {
		for (AggTransMaterialVO clientFullVO : clientBills) {
			clientFullVO.getParentVO().setAttributeValue("${vmObject.billstatus}",
					BillStatusEnum.COMMIT.value());
			clientFullVO.getParentVO().setStatus(VOStatus.UPDATED);
		}
		// 数据持久化
		AggTransMaterialVO[] returnVos = new BillUpdate<AggTransMaterialVO>().update(
				clientBills, originBills);
		return returnVos;
	}
}
