package nc.bs.uapbd.tranm.ace.bp;

import nc.impl.pubapp.pattern.data.bill.BillUpdate;
import nc.vo.pub.VOStatus;
import nc.vo.uapbd.tranm.AggTransMaterialVO;

/**
 * 标准单据审核的BP
 */
public class AceTranmApproveBP {

	/**
	 * 审核动作
	 * 
	 * @param vos
	 * @param script
	 * @return
	 */
	public AggTransMaterialVO[] approve(AggTransMaterialVO[] clientBills,
			AggTransMaterialVO[] originBills) {
		for (AggTransMaterialVO clientBill : clientBills) {
			clientBill.getParentVO().setStatus(VOStatus.UPDATED);
		}
		BillUpdate<AggTransMaterialVO> update = new BillUpdate<AggTransMaterialVO>();
		AggTransMaterialVO[] returnVos = update.update(clientBills, originBills);
		return returnVos;
	}

}
