package nc.bs.pub.action;

import nc.bs.framework.common.NCLocator;
import nc.bs.pubapp.pf.action.AbstractPfAction;
import nc.impl.pubapp.pattern.rule.IRule;
import nc.impl.pubapp.pattern.rule.processer.CompareAroundProcesser;
import nc.vo.jcom.lang.StringUtil;
import nc.vo.pub.BusinessException;
import nc.vo.pubapp.pattern.exception.ExceptionUtils;

import nc.bs.uapbd.tranm.plugin.bpplugin.TranmPluginPoint;
import nc.vo.uapbd.tranm.AggTransMaterialVO;
import nc.itf.uapbd.ITranmMaintain;

public class N_PC97_SAVEBASE extends AbstractPfAction<AggTransMaterialVO> {

	@Override
	protected CompareAroundProcesser<AggTransMaterialVO> getCompareAroundProcesserWithRules(
			Object userObj) {
		CompareAroundProcesser<AggTransMaterialVO> processor = null;
		AggTransMaterialVO[] clientFullVOs = (AggTransMaterialVO[]) this.getVos();
		if (!StringUtil.isEmptyWithTrim(clientFullVOs[0].getParentVO()
				.getPrimaryKey())) {
			processor = new CompareAroundProcesser<AggTransMaterialVO>(
					TranmPluginPoint.SCRIPT_UPDATE);
		} else {
			processor = new CompareAroundProcesser<AggTransMaterialVO>(
					TranmPluginPoint.SCRIPT_INSERT);
		}
		// TODO 在此处添加前后规则
		IRule<AggTransMaterialVO> rule = null;

		return processor;
	}

	@Override
	protected AggTransMaterialVO[] processBP(Object userObj,
			AggTransMaterialVO[] clientFullVOs, AggTransMaterialVO[] originBills) {

		AggTransMaterialVO[] bills = null;
		try {
			ITranmMaintain operator = NCLocator.getInstance()
					.lookup(ITranmMaintain.class);
			if (!StringUtil.isEmptyWithTrim(clientFullVOs[0].getParentVO()
					.getPrimaryKey())) {
				bills = operator.update(clientFullVOs, originBills);
			} else {
				bills = operator.insert(clientFullVOs, originBills);
			}
		} catch (BusinessException e) {
			ExceptionUtils.wrappBusinessException(e.getMessage());
		}
		return bills;
	}
}
