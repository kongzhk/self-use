package nc.bs.pub.action;

import nc.bs.framework.common.NCLocator;
import nc.bs.pubapp.pf.action.AbstractPfAction;
import nc.bs.pubapp.pub.rule.CommitStatusCheckRule;
import nc.impl.pubapp.pattern.rule.IRule;
import nc.impl.pubapp.pattern.rule.processer.CompareAroundProcesser;
import nc.vo.pub.BusinessException;
import nc.vo.pubapp.pattern.exception.ExceptionUtils;

import nc.bs.uapbd.tranm.plugin.bpplugin.TranmPluginPoint;
import nc.vo.uapbd.tranm.AggTransMaterialVO;
import nc.itf.uapbd.ITranmMaintain;

public class N_PC97_SAVE extends AbstractPfAction<AggTransMaterialVO> {

	protected CompareAroundProcesser<AggTransMaterialVO> getCompareAroundProcesserWithRules(
			Object userObj) {
		CompareAroundProcesser<AggTransMaterialVO> processor = new CompareAroundProcesser<AggTransMaterialVO>(
				TranmPluginPoint.SEND_APPROVE);
		// TODO 在此处添加审核前后规则
		IRule<AggTransMaterialVO> rule = new CommitStatusCheckRule();
		processor.addBeforeRule(rule);
		return processor;
	}

	@Override
	protected AggTransMaterialVO[] processBP(Object userObj,
			AggTransMaterialVO[] clientFullVOs, AggTransMaterialVO[] originBills) {
		ITranmMaintain operator = NCLocator.getInstance().lookup(
				ITranmMaintain.class);
		AggTransMaterialVO[] bills = null;
		try {
			bills = operator.save(clientFullVOs, originBills);
		} catch (BusinessException e) {
			ExceptionUtils.wrappBusinessException(e.getMessage());
		}
		return bills;
	}

}
