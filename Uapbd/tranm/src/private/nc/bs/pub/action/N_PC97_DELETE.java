package nc.bs.pub.action;

import nc.bs.framework.common.NCLocator;
import nc.bs.pubapp.pf.action.AbstractPfAction;
import nc.impl.pubapp.pattern.rule.processer.CompareAroundProcesser;
import nc.vo.pub.BusinessException;
import nc.vo.pubapp.pattern.exception.ExceptionUtils;

import nc.bs.uapbd.tranm.plugin.bpplugin.TranmPluginPoint;
import nc.vo.uapbd.tranm.AggTransMaterialVO;
import nc.itf.uapbd.ITranmMaintain;

public class N_PC97_DELETE extends AbstractPfAction<AggTransMaterialVO> {

	@Override
	protected CompareAroundProcesser<AggTransMaterialVO> getCompareAroundProcesserWithRules(
			Object userObj) {
		CompareAroundProcesser<AggTransMaterialVO> processor = new CompareAroundProcesser<AggTransMaterialVO>(
				TranmPluginPoint.SCRIPT_DELETE);
		// TODO 在此处添加前后规则
		return processor;
	}

	@Override
	protected AggTransMaterialVO[] processBP(Object userObj,
			AggTransMaterialVO[] clientFullVOs, AggTransMaterialVO[] originBills) {
		ITranmMaintain operator = NCLocator.getInstance().lookup(
				ITranmMaintain.class);
		try {
			operator.delete(clientFullVOs, originBills);
		} catch (BusinessException e) {
			ExceptionUtils.wrappBusinessException(e.getMessage());
		}
		return clientFullVOs;
	}

}
