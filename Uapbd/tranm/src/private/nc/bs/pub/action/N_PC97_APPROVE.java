package nc.bs.pub.action;

import nc.bs.framework.common.NCLocator;
import nc.bs.pubapp.pf.action.AbstractPfAction;
import nc.bs.pubapp.pub.rule.ApproveStatusCheckRule;
import nc.impl.pubapp.pattern.rule.IRule;
import nc.impl.pubapp.pattern.rule.processer.CompareAroundProcesser;
import nc.vo.pub.BusinessException;
import nc.vo.pubapp.pattern.exception.ExceptionUtils;

import nc.bs.uapbd.tranm.plugin.bpplugin.TranmPluginPoint;
import nc.vo.uapbd.tranm.AggTransMaterialVO;
import nc.itf.uapbd.ITranmMaintain;

public class N_PC97_APPROVE extends AbstractPfAction<AggTransMaterialVO> {

	public N_PC97_APPROVE() {
		super();
	}

	@Override
	protected CompareAroundProcesser<AggTransMaterialVO> getCompareAroundProcesserWithRules(
			Object userObj) {
		CompareAroundProcesser<AggTransMaterialVO> processor = new CompareAroundProcesser<AggTransMaterialVO>(
				TranmPluginPoint.APPROVE);
		processor.addBeforeRule(new ApproveStatusCheckRule());
		return processor;
	}

	@Override
	protected AggTransMaterialVO[] processBP(Object userObj,
			AggTransMaterialVO[] clientFullVOs, AggTransMaterialVO[] originBills) {
		AggTransMaterialVO[] bills = null;
		ITranmMaintain operator = NCLocator.getInstance().lookup(
				ITranmMaintain.class);
		try {
			bills = operator.approve(clientFullVOs, originBills);
		} catch (BusinessException e) {
			ExceptionUtils.wrappBusinessException(e.getMessage());
		}
		return bills;
	}

}
