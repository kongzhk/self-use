package nc.ui.uapbd.tranm.ace.serviceproxy;

import nc.bs.framework.common.NCLocator;
import nc.itf.uapbd.ITranmMaintain;
import nc.ui.pubapp.uif2app.query2.model.IQueryService;
import nc.ui.querytemplate.querytree.IQueryScheme;

/**
 * 示例单据的操作代理
 * 
 * @author author
 * @version tempProject version
 */
public class AceTranmMaintainProxy implements IQueryService {
	@Override
	public Object[] queryByQueryScheme(IQueryScheme queryScheme)
			throws Exception {
		ITranmMaintain query = NCLocator.getInstance().lookup(
				ITranmMaintain.class);
		return query.query(queryScheme);
	}

}